class LoginPage < SitePrism::Page
    set_url '/users/sign_in'
    element :user, '#user_email'
    element :password, '#user_password'
    element :sign_in, 'input[type=submit]'

    def with (u,s)
        self.user.set u
        self.password.set s 
        self.sign_in.click
    end
end 

class TaskPage < SitePrism::Page
    element :task_button, '.btn-success'
    element :task_description, 'input[id=new_task]'
    element :insert_task, '.input-group > span'
    
    def with(t)
        self.task_button.click
        self.task_description.set t
        self.insert_task.click
    end
end    

class SubtaskPage < SitePrism::Page
    element :subtask_button, '.btn-primary'
    element :subtask_description, 'input[id=new_sub_task]'
    element :insert_subtask, '#add-subtask'
    
    def with(s)
        self.subtask_button.click
        self.subtask_description.set s
        self.insert_subtask.click
    end
end  
