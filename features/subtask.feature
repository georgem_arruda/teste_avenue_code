@deletetask
Feature: SubTask Registration
As a ToDo App user
I should be able to create a subtask
So I can break down my tasks in smaller pieces

@authentication @logout
Scenario: Create SubTask
   Given I've registred a task
   When I register a subtask with:
        | Subtask | Reading at leat a chapter a day |
   Then I should see 'Reading the Bible this month'
