@deletetask
Feature: Tasks Registration
As a ToDo App user
I should be able to create a task
So I can manage my tasks

@authentication @logout
Scenario: Create Task
   Given I am logged with user Georgem Arruda
     And I see the message "Signed in successfully."
    When I register a task with:
        | Task | Reading the book "The book thief" until december |
    Then I should see 'Reading the book "The book thief" until december'
