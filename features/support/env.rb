require 'capybara'
require 'capybara/cucumber' 
require 'capybara/rspec/matchers' 
require 'selenium-webdriver'
require 'rspec'
require 'site_prism'



BROWSER = ENV['BROWSER']

Capybara.configure do |config|
    if BROWSER == 'chrome'
        config.default_driver = :selenium_chrome
    elsif BROWSER == 'headless'  
        config.default_driver = :selenium_chrome_headless
    else
        config.default_driver = :selenium
    end  
    Capybara.page.current_window.resize_to(1400,900)  
    config.app_host = 'https://qa-test.avenuecode.com'
end  


Capybara.default_max_wait_time = 30
Capybara.default_selector = :css
