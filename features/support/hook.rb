Before('@authentication') do 
    @login = LoginPage.new
    @login.load 
    @login.with('ton.arruda@hotmail.com', 'Dog005dade099')
end 

Before do    
    @inserttask = TaskPage.new
    @insertsubtask = SubtaskPage.new  
end

After('@logout') do   
    click_on('Sign out')
    retorno = find('.alert-info').text
    expect(retorno).to eql 'Signed out successfully.' 
end 

After('@deletetask') do
    find('.btn-danger').click
end
