#------GIVEN-------
Given("I am logged with user Georgem Arruda") do
    page.find('li', text: 'Welcome, Georgem Arruda!')
end

Given("I see the message {string}") do |message|
    retorno = find('.alert-info').text
    expect(retorno).to eql message
  end

Given("I've registred a task") do
    @inserttask.with('Reading the Bible this month')
end

#------WHEN-------
  
When("I register a task with:") do |table|
    @task = table.rows_hash['Task']
    @inserttask.with(@task)
end

When("I register a subtask with:") do |table|
    @subtask = table.rows_hash['Subtask']      
    @insertsubtask.with(@subtask)
    click_on('Close')
end
    
#------THEN-------

Then("I should see {string}") do |message|
    page.should have_content(message)
end
